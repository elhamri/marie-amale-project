package fr.eurecom.dsg.mapreduce;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 * 
 */
public class WordCount extends Configured implements Tool {

  private int numReducers;
  private Path inputPath;
  private Path outputDir;

  @Override
  public int run(String[] args) throws Exception {

    Configuration conf = this.getConf();
    Job job = new Job(conf,"Word Count"); // TODO: define new job instead of null using conf // DONE

    job.setInputFormatClass(TextInputFormat.class); // TODO: set job input format// DONE
    job.setOutputKeyClass(Text.class);
    job.setMapperClass(Map.class);
    job.setOutputValueClass(IntWritable.class); // TODO: set map class and the map output key and value classes// DONE
    job.setReducerClass(Reduce.class); // TODO: set reduce class and the reduce output key and value classes // DONE
    job.setOutputFormatClass(TextOutputFormat.class);// TODO: set job output format// DONE
    FileInputFormat.addInputPath(job, new Path(args[0]));// TODO: add the input file as job input (from HDFS) to the variable // //       inputPath // DONE
    FileOutputFormat.setOutputPath(job, new Path(args[1]));// TODO: set the output path for the job results (to HDFS) to the variable // DONE
    //       outputPath
   job.setNumReduceTasks(Integer.parseInt(args[0])); // TODO: set the number of reducers using variable numberReducers //DONE
   job.setJarByClass(WordCount.class); // TODO: set the jar class // DONE


        

    return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
  }
  
  public WordCount (String[] args) {
    if (args.length != 3) {
      System.out.println("Usage: WordCount <num_reducers> <input_path> <output_path>");
      System.exit(0);
    }
    this.numReducers = Integer.parseInt(args[0]);
    this.inputPath = new Path(args[1]);
    this.outputDir = new Path(args[2]);
  }
  
  public static void main(String args[]) throws Exception {
    int res = ToolRunner.run(new Configuration(), new WordCount(args), args);
    System.exit(res);
  }
}

class WCMapper extends Mapper <LongWritable, Text, Text, IntWritable> { // TODO: change Object to output value type // DONE
	
	private IntWritable ONE = new IntWritable(1);
        private Text textValue = new Text();

	@Override
  	
	protected void map(LongWritable key, // TODO: change Object to input key type // DONE
                     Text value, // TODO: change Object to input value type // DONE
                     Context context) throws IOException, InterruptedException {       
	
            String line = value.toString();
            String[] words = line.split("\\s+");
            for(String word : words) {
                textValue.set(word);
                context.write(textValue, ONE);}
    // TODO: implement the map method (use context.write to emit results) // DONE
  }



}

class WCReducer extends Reducer<Text key, // TODO: change Object to input key type // DONE
                                Iterable<IntWritable> values, // TODO: change Object to input value type //DONE
                                Text key, // TODO: change Object to output key type // DONE
                                Context context> { // TODO: change Object to output value type // DONE

  @Override

   protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws        IOException,    InterruptedException {
// TODO: implement the reduce method (use context.write to emit results) // DONE
  	int sum = 0;
 	 for (IntWritable val : values) 
		{
  		sum += val.get();
      		 }
	context.write(key, new IntWritable(sum));
		}


}